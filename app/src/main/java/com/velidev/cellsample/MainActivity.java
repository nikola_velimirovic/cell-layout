package com.velidev.cellsample;

import android.animation.LayoutTransition;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.velidev.celllayout.CellLayout;

public class MainActivity extends AppCompatActivity {

    private int position = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final CellLayout layout = (CellLayout) findViewById(R.id.cell_layout);
        final LayoutTransition transition = new LayoutTransition();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            transition.enableTransitionType(LayoutTransition.CHANGING);
        }

        layout.setLayoutTransition(transition);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] position = layout.getPosition(MainActivity.this.position);
                MainActivity.this.position = MainActivity.this.position == 1 ? 3 : 1;
                View child = layout.getChildAt(0);
                CellLayout.LayoutParams lp = (CellLayout.LayoutParams) child.getLayoutParams();
                lp.column = position[0];
                lp.row = position[1];
                child.setLayoutParams(lp);
            }
        });

        Button button = new Button(this);
        button.setText("I am big1");
//        button.setLayoutParams(new CellLayout.LayoutParams(0, 0, 2, 2));
        layout.addView(button);

        Button button1 = new Button(this);
        button1.setText("I am big2");
        button1.setLayoutParams(new CellLayout.LayoutParams(0, 2, 2, 2));
        layout.addView(button1);

        Button button2 = new Button(this);
        button2.setText("I am big3");
        button2.setLayoutParams(new CellLayout.LayoutParams(0, 0, 2, 2));
        layout.addView(button2);

        Button button3 = new Button(this);
        button3.setText("I am big4");
        button3.setLayoutParams(new CellLayout.LayoutParams(0, 2, 2, 2));
        layout.addView(button3);

    }
}
