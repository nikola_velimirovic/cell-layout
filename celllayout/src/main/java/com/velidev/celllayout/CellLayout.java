package com.velidev.celllayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikola on 10/8/2015.
 */
public class CellLayout extends ViewGroup {

    private static final String TAG = CellLayout.class.getSimpleName();
    List<View> scrap = new ArrayList<>();
    List<LayoutParams> oldParams = new ArrayList<>();
    private static Rect boundsCache = new Rect();
    protected int columnCount = 4, rowCount = 4;
    protected int cellWidth, cellHeight;
    protected boolean squareChildren = false;
    protected boolean reorderToFit = true;

    public CellLayout(Context context) {
        super(context);
        init(null);
    }

    public CellLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CellLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CellLayout);
            columnCount = a.getInteger(R.styleable.CellLayout_columnCount, columnCount);
            rowCount = a.getInteger(R.styleable.CellLayout_rowCount, rowCount);
            rowCount = a.getInteger(R.styleable.CellLayout_rowCount, rowCount);
            reorderToFit = a.getBoolean(R.styleable.CellLayout_reorderToFit, reorderToFit);
            squareChildren = a.getBoolean(R.styleable.CellLayout_squareChildren, false);
            a.recycle();
        }
    }

    @Override
    public void addView(final View child) {
        if (reorderToFit) {
            boolean handled = prepareForAdd(child);
            if (handled) return;
        }
        super.addView(child);
        Log.d(TAG, "added view ccount: " + getChildCount());
    }

    private boolean prepareForAdd(View child) {
        LayoutParams params = (LayoutParams) child.getLayoutParams();
        if (params == null) return false;
        int column = params.column;
        int row = params.row;
        int colSpan = params.colSpan;
        int rowSpan = params.rowSpan;
        int[] position = new int[]{column, row, colSpan, rowSpan};
        boolean free = isPositionFree(params.getBounds());
        if (!free) {
            int[] freePosition = getNextFreePosition(colSpan, rowSpan);
            if (freePosition != null) {
                params.column = freePosition[0];
                params.row = freePosition[1];
                params.colSpan = freePosition[2];
                params.rowSpan = freePosition[3];
                child.setLayoutParams(params);
            } else {
                int freeCellCount = getFreeCellCount();
                //TODO try to lift all views, remove position info and put them back in
                if (freeCellCount >= params.getSize()) {
                    detachToScrap(child);

                    int addedCount = 0;
                    //step 3. add them back
                    for (View view : scrap) {
                        LayoutParams p = (LayoutParams) view.getLayoutParams();
                        int[] horPos = findFirstFreeHorisontalPosition(p.colSpan, p.rowSpan);
                        if (horPos != null) {
                            p.column = horPos[0];
                            p.row = horPos[1];
                            addView(view, p);
                            addedCount++;
                        }
                    }

                    if (addedCount < scrap.size()) {
                        //step 4. remove all views and reatach
                        removeAllViews();
                        for (int i = 0; i < scrap.size(); i++) {
                            if (i == scrap.size() - 1) break;
                            View view = scrap.get(i);
                            LayoutParams p = (LayoutParams) view.getLayoutParams();
                            addView(view, p);
                        }
                        throw new IllegalArgumentException("No space for view which size is " + params.getSize());
                    } else {
                        return true;
                    }

                } else {
                    throw new IllegalArgumentException("No space for view which size is " + params.getSize());
                }
            }
        }
        return false;
    }

    private void detachToScrap(View view) {
        scrap.clear();
        oldParams.clear();

        int childCount = getChildCount();
        Log.d(TAG, "detachToScrap ccount: " + childCount);
        //step 1. detach all views and put into scrap
        for (int i = 0; i < childCount; i++) {
            Log.d(TAG, "detaching at " + i);
            View scrapView = getChildAt(i);
            LayoutParams sParams = (LayoutParams) scrapView.getLayoutParams();
            scrap.add(scrapView);
            oldParams.add(sParams.cloneNoException());
            Log.d(TAG, "detach: " + sParams);
        }

        if (view != null) {
            LayoutParams params = (LayoutParams) view.getLayoutParams();
            scrap.add(view);
            oldParams.add(params);
        }

        removeAllViews();
    }

    public int[] findFirstFreeHorisontalPosition(int colSpan, int rowSpan) {
        //kako ovo naci
        Log.d(TAG, "findFirstFreeHorisontalPosition cspan: " + colSpan + " rspan: " + rowSpan);
        Log.d(TAG, "________________________________________________________________________");
        int columnCount = getColumnCount();
        for (int r = 0; r < getRowCount(); r++) {
            for (int i = 0; i < columnCount; i++) {
                int posId = r * columnCount + i;
                int[] position = getPosition(posId, colSpan, rowSpan);
                boolean positionFree = isPositionFree(position);
                boolean outOfBounds = isOutOfBounds(position);

                Log.d(TAG, "r: " + r + " i: " + i + " posId: " + posId +
                                " position, col: " + position[0] + " row: " + position[1] + " cspan: " + position[2] + " rspan: " + position[3]
                                + " free: " + positionFree + " oob: " + outOfBounds
                );
                if (outOfBounds) {
                    break;
                }
                if (positionFree && !outOfBounds) {
                    Log.e(TAG, "found hor position col: " + position[0] + " row: " + position[1] + " cspan: " + position[2] + " rspan: " + position[3]);
                    return position;
                }
            }
        }
        return null;
    }

    public int getFreeCellCount() {
        int cellCount = getCellCount();
        for (int i = 0; i < getChildCount(); i++) {
            View view = getChildAt(i);
            LayoutParams params = (LayoutParams) view.getLayoutParams();
            cellCount -= params.getSize();
        }
        return cellCount;
    }

    public int getIndex(int row, int column) {
        return row * columnCount + column;
    }

    public boolean isPositionFree(int[] position) {
        Rect srcBounds = new Rect(position[0], position[1], position[0] + position[2], position[1] + position[3]);
        return isPositionFree(srcBounds);
    }

    public boolean isOutOfBounds(int[] position) {
        int colCount = getColumnCount();
        //4, 2, 3
        //colCount - column - colSpan
        boolean coob = colCount - position[0] - position[2] < 0;
        boolean roob = rowCount - position[1] - position[3] < 0;
        return coob || roob;
    }

    public boolean isPositionFree(Rect srcBounds) {
        for (int j = 0; j < getChildCount(); j++) {
            View view = getChildAt(j);
            LayoutParams lp = (LayoutParams) view.getLayoutParams();
            Rect destBounds = lp.getBounds();
            if (srcBounds.intersect(destBounds)) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                LayoutParams lp = (LayoutParams) child.getLayoutParams();
                lp.calculateSize(cellWidth, cellHeight);
                int childLeft = lp.column * cellWidth;
                int childTop = lp.row * cellHeight;
                int childRight = childLeft + lp.width;
                int childBottom = childTop + lp.height;
                child.layout(childLeft, childTop, childRight, childBottom);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);

        cellWidth = (widthSpecSize - getPaddingRight() - getPaddingLeft()) / columnCount;
        cellHeight = (heightSpecSize - getPaddingTop() - getPaddingBottom()) / rowCount;

        if (squareChildren) {
            if (cellHeight < cellWidth)
                cellWidth = cellHeight;
            else
                cellHeight = cellWidth;
        }

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            LayoutParams lp = (LayoutParams) child.getLayoutParams();
            lp.calculateSize(cellWidth, cellHeight);
            int childWidthMeasureSpec = MeasureSpec.makeMeasureSpec(lp.width, MeasureSpec.EXACTLY);
            int childheightMeasureSpec = MeasureSpec.makeMeasureSpec(lp.height, MeasureSpec.EXACTLY);
            child.measure(childWidthMeasureSpec, childheightMeasureSpec);
        }

        setMeasuredDimension(widthSpecSize, heightSpecSize);
    }

    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        int[] freePosition = getNextFreePosition(1, 1);
        return new LayoutParams(freePosition[0], freePosition[1], 1, 1);
    }

    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CellLayout);
            int columnSpan = a.getInteger(R.styleable.CellLayout_columnSpan, 1);
            int rowSpan = a.getInteger(R.styleable.CellLayout_rowSpan, 1);
            int[] freePosition = getNextFreePosition(columnSpan, rowSpan);
            int column = a.getInteger(R.styleable.CellLayout_column, freePosition[0]);
            int row = a.getInteger(R.styleable.CellLayout_row, freePosition[1]);
            a.recycle();
            return new LayoutParams(column, row, columnSpan, rowSpan);
        } else {
            return generateDefaultLayoutParams();
        }
    }

    public int[] getNextFreePosition(int columnSpan, int rowSpan) {
        int cellCount = getCellCount();
        Rect srcBounds = new Rect(0, 0, columnSpan, rowSpan);
        int[] res = null;
        for (int i = 0; i < cellCount; i++) {
            boolean positionFree = true;
            int[] position = getPosition(i, columnSpan, rowSpan);
            if (isOutOfBounds(position)) continue;

            srcBounds.left = position[0];
            srcBounds.top = position[1];
            srcBounds.right = srcBounds.left + position[2];
            srcBounds.bottom = srcBounds.top + position[3];

            for (int j = 0; j < getChildCount(); j++) {
                View view = getChildAt(j);
                LayoutParams lp = (LayoutParams) view.getLayoutParams();
                Rect destBounds = lp.getBounds();
                if (srcBounds.intersect(destBounds)) {
                    positionFree = false;
                    break;
                }
            }

            if (positionFree) {
                res = position;
                break;
            }
        }
        return res;
    }

    public View find(int x, int y) {
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (isViewInBounds(child, x, y)) return child;
        }
        return null;
    }

    private boolean isViewInBounds(View view, int rx, int ry) {
        int[] l = new int[2];
        view.getLocationOnScreen(l);
        int x = l[0];
        int y = l[1];
        int w = view.getWidth();
        int h = view.getHeight();

        if (rx < x || rx > x + w || ry < y || ry > y + h) {
            return false;
        }
        return true;
    }

    public int getCellCount() {
        return columnCount * rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
        invalidate();
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
        invalidate();
    }

    public int getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(int cellWidth) {
        this.cellWidth = cellWidth;
        invalidate();
    }

    public int getCellHeight() {
        return cellHeight;
    }

    public void setCellHeight(int cellHeight) {
        this.cellHeight = cellHeight;
        invalidate();
    }

    public int[] getPosition(int n, int colSpan, int rowSpan) {
        int column = n % columnCount;
        int row = n / columnCount;
        return new int[]{column, row, colSpan, rowSpan};
    }

    public int[] getPosition(int n) {
        return getPosition(n, 1, 1);
    }

    public static class LayoutParams extends ViewGroup.LayoutParams {
        public int rowSpan = 1;
        public int colSpan = 1;
        public int row = -1;
        public int column = -1;

        public LayoutParams(int column, int row, int colSpan, int rowSpan) {
            super(0, 0);
            this.colSpan = colSpan;
            this.rowSpan = rowSpan;
            this.column = column;
            this.row = row;
        }

        public void calculateSize(int cellWidth, int cellHeight) {
            if (row < 0 || column < 0) return;
            width = colSpan * cellWidth;
            height = rowSpan * cellHeight;
        }

        public Rect getBounds() {
            return new Rect(column, row, column + colSpan, row + rowSpan);
        }

        public int getSize() {
            return colSpan * rowSpan;
        }

        public LayoutParams cloneNoException() {
            return new LayoutParams(column, row, colSpan, rowSpan);
        }

        @Override
        public String toString() {
            return "LayoutParams{" +
                    "rowSpan=" + rowSpan +
                    ", colSpan=" + colSpan +
                    ", row=" + row +
                    ", column=" + column +
                    '}';
        }
    }

}
